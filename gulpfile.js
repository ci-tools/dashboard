const gulp          = require('gulp')
const concat        = require('gulp-concat')
const log           = require('gulplog')

const watchify      = require('watchify')
const pump          = require('pump')

const browserify    = require('browserify')
const source        = require('vinyl-source-stream')
const assign        = require('lodash/assign')

const bsync         = require('browser-sync')

/* Variables and options */
const paths = {
    'css': [
        'src/css/*.css'
    ],
    'js': {
        'app': 'src/js/**/*.js',
        'entry_point': 'src/js/index.js'
    }
}

const browserify_opts = {
    entries: [ paths.js.entry_point ],
    extensions: ['.js'],
    debug: true
}
const options = assign({}, watchify.args, browserify_opts)

let base = browserify(options)

gulp.task('css', css_bundler);
gulp.task('js', js_bundler);

gulp.task('default', gulp.parallel('js', 'css'));

gulp.task('serve', () => {
    bsync({
        server: '.'
    })

    let build = watchify(base)
    build.on('update', () => js_bundler(build))
    build.on('log', log.info)
})

gulp.task('watch', gulp.series('default', 'serve'))

function css_bundler (cb) {
    pump( [
        gulp.src(paths.css),
        concat('style.min.css'),
        gulp.dest("assets/css/"),
        bsync.reload( { stream: true } )
    ], cb)
}

function js_bundler (cb, build) {
    let b = build ? build : base
    pump( [
        b.bundle()
        .on('error', log.error.bind(log, 'Browserify Error')),

        source('bundle.min.js'),
        gulp.dest("assets/js/"),
        bsync.reload( { stream: true } )
    ], cb)
}
